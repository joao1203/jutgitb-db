import kotlinx.serialization.Serializable

@Serializable
data class Question(
    val numberID: Int,
    val title: String,
    val statement: String,
    val inputMeasures: String,
    val outputMeasures: String,
    val publicPlaytest: Playtest,
    val privatePlaytest: Playtest,
    var isResolved: Boolean,
    var tries: MutableList<String>
)
