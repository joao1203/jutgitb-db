import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.postgresql.util.PSQLException
import java.sql.DriverManager
import java.sql.SQLException
import java.util.Scanner

val sc = Scanner(System.`in`)
const val reset = "\u001B[0m"
const val bold = "\u001b[1m"
const val underline = "\u001b[4m"
const val framed = "\u001b[51m"
const val magenta = "\u001b[35;1m"
const val cyan = "\u001b[36;1m"
const val red = "\u001b[31;1m"
const val yellow = "\u001b[33;1m"
const val green = "\u001b[32m"
const val brightGreen = "\u001b[32;1m"

@Serializable
class Questions{
//    init {
//        for (i in readJsonFile){
//            val obj = Json.decodeFromString<Question>(i)
//            quest.add(obj)
//        }
//    }
    val runAnswer = mutableListOf<String>()

    fun printQuestion (q: Int){
        println("${underline}${bold}Question ${quest[q].numberID}: ${quest[q].title}$reset \n")
        println(" ${red}${framed} Description: ${reset}\n   ${quest[q].statement}")
        println("\n ${magenta}${framed} Input: ${reset}\n   ${quest[q].inputMeasures}")
        println("\n ${cyan}${framed} Output: ${reset}\n   ${quest[q].outputMeasures}")
        println("\n ${yellow}${framed} Example: $reset")
        println("   Input:  ${quest[q].publicPlaytest.input}")
        println("   Output: ${quest[q].publicPlaytest.output}\n")
    }

    fun resolveQuestion (q: Int){
        println("\n ${green}${framed} Private Playground: $reset")
        println("   Input: ${quest[q].privatePlaytest.input}")
        println("   Output: ${bold}???${reset}")
        do {
            println("\n  Write the correct output:")
            val userAnswer = sc.nextLine()

            if (userAnswer != "cancel" && quest[q].privatePlaytest.output != userAnswer.lowercase()){
                quest[q].tries
                quest[q].tries.add(userAnswer)
                runAnswer.add(userAnswer)
                updateFile(quest[q].numberID, quest[q].tries, false)
            }
            if (quest[q].privatePlaytest.output == userAnswer.lowercase()) {
                quest[q].tries
                quest[q].tries.add(userAnswer)
                runAnswer.add(userAnswer)
                quest[q].isResolved = true
                updateFile(quest[q].numberID, quest[q].tries, true)
                println("\n  ${bold}Congratulations, $userAnswer is the correct output!$reset")
            }
            else println("  I'm sorry, that is not the correct output...")

        } while (!quest[q].isResolved && userAnswer != "cancel")
    }

    fun printState(q: Int){
        println("\n  Number of tries: ${quest[q].tries.size}")

        if (!quest[q].isResolved){
            println("  ${red}${framed} User Attempts: $reset")

            for (item in quest[q].tries.indices){
                println("  ${red}->${reset} ${quest[q].tries[item]}")
            }
            println("  Solved: ${quest[q].isResolved.toString().uppercase()}\n")
        }
        else {
            println("  ${brightGreen}${framed} User Attempts: $reset")

            for (item in quest[q].tries.indices){
                println("  ${brightGreen}->${reset} ${quest[q].tries[item]}")
            }
            println("  Solved: ${quest[q].isResolved.toString().uppercase()}\n")
        }
    }

    private fun updateFile(id: Int, userTries: MutableList<String>, finish: Boolean){
        val sentenciaUpdate = "UPDATE problem SET resolved = ?, tries = ? WHERE id_problem = ?"
        val consultaUpdate = connect.prepareStatement(sentenciaUpdate)
        consultaUpdate.setBoolean(1,finish)
        consultaUpdate.setInt(2,userTries.size)
        consultaUpdate.setInt(3,id)
        consultaUpdate.executeUpdate()

        var numCount = 1
        val consultaTotal = connect.prepareStatement("SELECT * FROM tries")
        val test = consultaTotal.executeQuery()
        while (test.next()){
            numCount++
        }

        for (i in userTries.indices){
            if (userTries[i] in runAnswer){
                val sentenciaTry = "INSERT INTO tries VALUES (?, ?, ?)"
                val consultaTry = connect.prepareStatement(sentenciaTry)
                consultaTry.setInt(1, numCount++)
                consultaTry.setString(2, userTries[i])
                consultaTry.setInt(3,id)
                consultaTry.executeUpdate()
                runAnswer.clear()
            }
        }
//        val jsonQuest = jsonFile.readLines()
//        jsonFile.writeText("")
//        for (i in jsonQuest.indices){
//            val reformedQuest = Json.decodeFromString<Question>(jsonQuest[i])
//            if (reformedQuest.numberID == id){
//                reformedQuest.isResolved = finish
//                reformedQuest.tries = userTries
//            }
//            val objModified = Json.encodeToString(reformedQuest)
//            jsonFile.appendText("$objModified\n")
//        }
    }
}