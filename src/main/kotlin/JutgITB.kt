/**
 * @author Joao Victor Lopes Dias
 * @version 3.0
 */
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.postgresql.util.PSQLException
import java.io.File
import java.lang.IndexOutOfBoundsException
import java.math.RoundingMode
import java.sql.DriverManager
import java.sql.SQLException
import java.text.DecimalFormat

val quest = mutableListOf<Question>()
var solvedQuestions: Int = 0
val jdbcUrl = "jdbc:postgresql://localhost:5432/jutge"
val connect = DriverManager.getConnection(jdbcUrl, "postgres", "database69")

fun main(){
    //If running the program for the first time make sure to execute insertDatainTable() before running any other function
    loginUser()
}

/**
 * Function that queries the database and converts the results into a list of [Question] objects.
 * If any SQL errors occur during the process, the error is printed to the console.
 */
fun databaseToList(){
    try {
        val query = connect.prepareStatement("SELECT * FROM problem ORDER BY id_problem ASC")

        val result = query.executeQuery()

        while (result.next()){
            val numberID = result.getInt("id_problem")
            val title = result.getString("title")
            val statement = result.getString("statement")
            val inputMeasures = result.getString("inputmeasures")
            val outputMeasures = result.getString("inputmeasures")
            val public_input = result.getString("public_input")
            val public_output = result.getString("public_output")
            val private_input = result.getString("private_input")
            val private_output = result.getString("private_output")
            var resolve = result.getBoolean("resolved")
            var tries = mutableListOf<String>()

            val queryTries = connect.prepareStatement("SELECT * FROM tries WHERE id_problem = ?")
            queryTries.setInt(1,numberID)
            val triesresult = queryTries.executeQuery()
            while (triesresult.next()){
                val attempt = triesresult.getString("try")
                tries.add(attempt)
            }
            quest.add(Question(numberID,title,statement,inputMeasures,outputMeasures,Playtest(public_input,public_output),Playtest(private_input,private_output),resolve, tries))
        }
    }catch (e: SQLException){
        println("Error:" + e.errorCode + e.message)
    } catch (e: PSQLException){
        println("Error:" + e.errorCode + e.message)
    }
}

/**
 * Function that prompts the user to enter their role as a student or teacher, and calls the corresponding menu function based on the input.
 */
fun loginUser(){
    println("${underline}${bold}Welcome to JutgITB 3.0!${reset}")
    println("Identify yourself:")
    println(" ·Student\n ·Teacher")
    do {
        val userEnter = sc.next().uppercase()
        when(userEnter){
            "STUDENT" -> studentMenu()
            "TEACHER" -> teacherLogin()
            else -> println("$userEnter is not a valid option, try again!")
        }
    }while (userEnter != "STUDENT" && userEnter != "TEACHER")
}

/**
 * Function that displays the Student Menu with five options and prompts the user for their choice.
 * Options:
 * Follow the learning itinerary - calls [solveQuestionsDatabase] function to start solving questions
 * Question List - calls [checkListDatabase] function to display a list of questions
 * Check solved questions - calls [checkSolvedDatabase] function to display previously solved questions and allows the user to revisit them
 * Help - calls [giveHelp] function to display a brief explanation of the program and allows the user to resume solving questions
 * Exit - exits the program
 * If an invalid choice is made, an error message is displayed and the menu is shown again until a valid option is chosen.
 */
fun studentMenu(){
    println("${underline}${bold}Student Menu${reset}")
    println(" 1·Follow the learning itinerary\n" +
            " 2·Question List\n" +
            " 3·Check solved questions\n" +
            " 4·Help\n" +
            " 5·Exit")
    while (true){
        when (val sChoice = sc.nextInt()){
            1 -> solveQuestionsDatabase()
            2 -> checkListDatabase()
            3 -> checkSolvedDatabase()
            4 -> giveHelp()
            5 -> exit()
            else -> println("$sChoice is not a valid option, try again!")
        }
    }
}

/**
 * Function that retrieves the list of questions from the database and allows the user to solve them.
 * It prints out each question, prompts the user to solve it, and prints the state of the question.
 * Once all the questions have been shown, it prints out the number of questions solved and returns to the main menu.
 */
fun solveQuestionsDatabase(){
    databaseToList()
    sc.nextLine()
    for (q in quest.indices){
        if (!quest[q].isResolved){
            Questions().printQuestion(q)
            println("  ${underline}Would you like to solve this question?${reset}\n   1.Yes\n   2.No")

            do {
                var userChoice = sc.nextLine()
                if (userChoice == "1"){
                    Questions().resolveQuestion(q)
                    Questions().printState(q)
                    userChoice = "2"
                }
                else if (userChoice == "2"){
                    if (q != quest.size-1){
                        println("Understood! Next question incoming!!!\n")
                    }
                }
                else {println("  $userChoice is not a valid option, try again!")}
            }while (userChoice != "2")
        }
    }

    for (i in quest.indices){
        if (quest[i].isResolved){
            solvedQuestions++
        }
    }
    println("\n  ${bold}You've solved $solvedQuestions out of ${quest.size} questions!${reset}\n")
    println("${underline}${bold}Student Menu${reset}")
    println(" 1·Follow the learning itinerary\n" +
            " 2·Question List\n" +
            " 3·Check solved questions\n" +
            " 4·Help\n" +
            " 5·Exit")
}

/**
 * Function that displays a list of all questions in the database.
 * Allows the user to choose a question and solve it.
 * If the chosen question is not in the database, an error message is printed and the program exits.
 * @throws IndexOutOfBoundsException if the userChoice is invalid (not within the database's range of questions)
 */
fun checkListDatabase(){
    databaseToList()
    for (q in quest.indices){
        println("${bold}Question ${quest[q].numberID}:${reset} ${quest[q].title}")
    }
    val userChoice = sc.nextInt()-1
    try {
        sc.nextLine()
        quest[userChoice].isResolved = false
        Questions().printQuestion(userChoice)
        Questions().resolveQuestion(userChoice)
        Questions().printState(userChoice)
    }catch (e: IndexOutOfBoundsException){
        println("Question ${userChoice+1} not found!")
    }
    exit()
}

/**
 * Function that reads the database and prints the list of questions that have been previously attempted or solved by the student.
 * If the student selects a question that has been previously attempted, the function will display the question again and allow the student to solve it again.
 * If the student selects a question that has not been previously attempted or does not exist, the function will print an error message and exit the program.
 */
fun checkSolvedDatabase(){
    databaseToList()
    val checkedQuestions = mutableListOf<Int>()
    for (q in quest.indices){
        if (quest[q].tries.size > 0){
            println("Question ${quest[q].numberID}: ${quest[q].title}")
            Questions().printState(q)
            checkedQuestions.add(quest[q].numberID)
        }
    }
    val userChoice = sc.nextInt()
    sc.nextLine()
    if (userChoice in checkedQuestions){
        quest[userChoice].isResolved = false
        Questions().printQuestion(userChoice-1)
        Questions().resolveQuestion(userChoice-1)
        Questions().printState(userChoice-1)
    }
    else {
        println("Question $userChoice not found!")
        exit()
    }
}

/**
 * Function that prints a brief explanation of the program and its usage to help students learn programming.
 */
fun giveHelp(){
    println("Students can learn programming by using this terminal-based teaching program to practice and solve coding questions.")
    println("They can use the menu to access the learning itinerary, go over previously solved questions or read this brief explanation.")
    println("\nWould you like to resume solving the questions?")
    println(" 1·Yes\n 2·No")
    val choice = sc.nextInt()
    if (choice == 1){
        solveQuestionsDatabase()
    }
    else exit()
}

/**
 * Function that exits the program.
 */
fun exit(){
    kotlin.system.exitProcess(0)
}

/**
 * Function that asks for the teacher's password and grants access to the teacher menu if the correct password is entered (The password is "itb.cat").
 * If the user enters the wrong password three times, the program exits.
 */
fun teacherLogin(){
    var attempts = 3
    var password: String
    println("Insert teachers password: (itb.cat)")
    do {
        password = sc.next()
        if (password != "itb.cat"){
            attempts--
        }
    }while (attempts > 0 && password != "itb.cat")

    if (attempts == 0){
        println("Out of attempts!")
        exit()
    }
    if (password == "itb.cat"){
        teacherMenu()
    }
}

/**
 * Function displays the teacher menu.
 */
fun teacherMenu(){
    println("${underline}${bold}Teacher Menu${reset}")
    println(" 1·Add new problems\n" +
            " 2·Review students progress\n" +
            " 3·Exit")
    while (true){
        when (val teacherEnter = sc.nextInt()){
            1 -> addQuestionDatabase()
            2 -> gradesNview()
            3 -> exit()
            else -> println("$teacherEnter is not a valid option, try again!")
        }
    }
}

/**
 * Function that adds a new question to the database by prompting the user for the necessary information
 * and creating a new Question object with said information. The new question is then added to
 * the database table 'problem' using a prepared statement.
 * @throws SQLException if there is an error executing the SQL query
 * @throws PSQLException if there is an error with the PostgreSQL syntax
 */
fun addQuestionDatabase(){
    sc.nextLine()
    println("Write the title of the new question:")
    val titleNew = sc.nextLine()
    println("Write the statement of the new question:")
    val statNew = sc.nextLine()
    println("Write the input measure of the new question:")
    val inputNew = sc.nextLine()
    println("Write the output measure of the new question:")
    val outputNew = sc.nextLine()
    println("Write the public input of the new question:")
    val publicInput = sc.nextLine()
    println("Write the public output of the new question:")
    val publicOutput = sc.nextLine()
    println("Write the private input of the new question:")
    val privateInput = sc.nextLine()
    println("Write the private output of the new question:")
    val privateOutput = sc.nextLine()

    var numCount = 1
    val consultaTotal = connect.prepareStatement("SELECT * FROM problem")
    val test = consultaTotal.executeQuery()
    while (test.next()){
        numCount++
    }

    val newQuestion = Question(numCount++,titleNew,statNew,inputNew,outputNew,Playtest(publicInput,publicOutput),Playtest(privateInput,privateOutput),false, mutableListOf())

    try {
        val sentenciaProblem = "INSERT INTO problem VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

        val consultaProblem = connect.prepareStatement(sentenciaProblem)

        consultaProblem.setInt(1, newQuestion.numberID)
        consultaProblem.setString(2, newQuestion.title)
        consultaProblem.setString(3, newQuestion.statement)
        consultaProblem.setString(4, newQuestion.inputMeasures)
        consultaProblem.setString(5, newQuestion.outputMeasures)
        consultaProblem.setString(6, newQuestion.publicPlaytest.input)
        consultaProblem.setString(7, newQuestion.publicPlaytest.output)
        consultaProblem.setString(8, newQuestion.privatePlaytest.input)
        consultaProblem.setString(9, newQuestion.privatePlaytest.output)
        consultaProblem.setBoolean(10, newQuestion.isResolved)
        consultaProblem.setInt(11, newQuestion.tries.size)
        consultaProblem.executeUpdate()
    }catch (e: SQLException){
        println("Error:" + e.errorCode + e.message)
    } catch (e: PSQLException){
        println("Error:" + e.errorCode + e.message)
    }
}

/**
 * Function that displays the menu for reviewing student progress, with options to get student scores, penalize students,
 * show progress or exit the program.
 */
fun gradesNview(){
    println("${bold}Review students progress!${reset}")
    println(" 1·Get students score\n" + " 2·Penalize student\n" + " 3·Show progress\n" + " 4·Exit")
    while (true){
        when(val userEnter = sc.nextInt()){
            1 -> getStuScoreDatabase()
            2 -> penalizeStudentDatabase()
            3 -> showResultsDatabase()
            4 -> exit()
            else -> println("$userEnter is not a valid option, try again!")
        }
    }
}

/**
 * Function that calculates the student's score based on the number of questions they solved.
 */
fun getStuScoreDatabase(){
    databaseToList()
    var stuScore = 0.0
    for (i in quest.indices){
        if (quest[i].isResolved){
            stuScore += 1.0
            solvedQuestions++
        }
    }
    stuScore /= quest.size
    stuScore *= 10

    val df = DecimalFormat("#.##")
    df.roundingMode = RoundingMode.DOWN
    val roundoff = df.format(stuScore)

    println("The student solved $solvedQuestions out of ${quest.size} questions, their grade is $roundoff")
    println("\n 1·Get students score\n" + " 2·Penalize student\n" + " 3·Show progress\n" + " 4·Exit")
}

/**
 * Function that penalizes the student's score by decreasing it based on the number of tries they took for each solved question.
 */
fun penalizeStudentDatabase(){
    databaseToList()
    var stuScore = 0.0
    for (i in quest.indices){
        if (quest[i].isResolved){
            stuScore += when(quest[i].tries.size){
                1 -> 1.0
                2 -> 0.90
                3 -> 0.80
                4 -> 0.70
                5 -> 0.60
                6 -> 0.50
                else -> 0.25
            }
            solvedQuestions++
        }
    }
    stuScore /= quest.size
    stuScore *= 10

    val df = DecimalFormat("#.##")
    df.roundingMode = RoundingMode.DOWN
    val roundoff = df.format(stuScore)

    println("The student solved $solvedQuestions out of ${quest.size} questions, and after penalization their grade is $roundoff")
    println("\n 1·Get students score\n" + " 2·Penalize student\n" + " 3·Show progress\n" + " 4·Exit")
}

/**
 * Function that displays a list of questions with their associated tries and if they are solved or not.
 */
fun showResultsDatabase(){
    databaseToList()
    for (q in quest.indices){
        if (quest[q].tries.size > 0){
            println("Question ${quest[q].numberID}: ${quest[q].title}")
            println("Tries:${quest[q].tries.size}")
            println("Solved:${quest[q].isResolved}")
        }
    }
    println("\n 1·Get students score\n" + " 2·Penalize student\n" + " 3·Show progress\n" + " 4·Exit")
}

//Code used to insert the original values into the database and its tables
val jsonFile = File("src/main/kotlin/questions.json")
val readJsonFile = jsonFile.readLines()
val quest1 = mutableListOf<Question>()

fun jsonToList(){
    for (i in readJsonFile){
        val obj = Json.decodeFromString<Question>(i)
        quest.add(obj)
    }
}
fun insertDataInTable(){
    val questionsLists = listOf<Question>(
        Question(1,"Double the integer!","Write a program that reads an input number and outputs its double.","The input will be an integer number.","The output will be an integer that corresponds to the double value of the input integer.",Playtest("25","50"),Playtest("33","66"), false, mutableListOf()),
        Question(2,"Sum of two integer numbers!","Write a program that when given a set of two integers returns their sum.","The input will be two integer numbers.","The output will be an integer that corresponds to the sum of the input integers.",Playtest("12 9","21"),Playtest("13 19","32"), false, mutableListOf()),
        Question(3,"Calculate the discount!","Write a program that reads the original price, the current price and outputs the discount percentage.","The input will be the two prices, the first one should be the original price followed by the current price.","The output will be a decimal value that corresponds to the discount applied to the price (Float value).",Playtest("600 450","25.00"),Playtest("800 750","6.25"), false, mutableListOf()),
        Question(4,"Is of legal age?","Write a program that reads the input received by the user and checks if the user is of legal age.","The input will be an integer value that represents the users age.","The output will be a boolean value, true if the user is of legal age and false if they are underage.",Playtest("27","true"),Playtest("12","false"), false, mutableListOf()),
        Question(5,"How much time?","Write a program that calculates how many hours, minutes and seconds there are in a given amount of seconds.","The input will be an integer value that corresponds to an amount of seconds","The output will be the number of hours, minutes and seconds that the given amount of seconds equals to.",Playtest("34532","9 hours 35 minutes 32 seconds"),Playtest("47589","13 hours 13 minutes 9 seconds"), false, mutableListOf()),
        Question(6,"Biggest of 3 numbers!","Write a program that when given three numbers, prints the biggest of them.","The input will be the integer numbers.","The output will be the biggest of the previous three integer numbers.",Playtest("23 57 38","57"),Playtest("43 69 82","82"), false, mutableListOf()),
        Question(7,"Even or odd?","Write a program that when given a number prints out if its even or odd.","The input will be an integer number.","The output will be if the given integer is an even or odd number.",Playtest("12","even"),Playtest("37","odd"), false, mutableListOf()),
        Question(8,"Calculator!","Write a program that when given two integer numbers and an operation symbol, executes the operation and returns the result.","The input will be two integer numbers and one of the following symbols (+, -, *, /, %).","The output will be the result of the operation.",Playtest("23 25 +", "48"), Playtest("12 4 *", "48"), false, mutableListOf()),
        Question(9,"How many days does the month have?","Write a program that when given a number that corresponds to one of the months of the year responds with the amount of days in said month.","The input will be an integer value.","The output will be the amount of days in the chosen month.",Playtest("3","31"),Playtest("2","28"), false, mutableListOf()),
        Question(10,"Absolute value!","Write a program that when given a number returns its absolute value.","The input will be an integer number.","The output will be the absolute value of the given integer",Playtest("-34","34"),Playtest("57","57"), false, mutableListOf()),
        Question(11,"Print the range!","Write a program that when given two integer numbers prints out the sequence between the two of them.","The input will be two integer numbers.","The output will be the sequence of numbers between the integer numbers, all within the same line.",Playtest("2 7","2,3,4,5,6,7"),Playtest("25 19","25,24,23,22,21,20,19"), false, mutableListOf()),
        Question(12,"Raise it!","Write a program that when given two integer numbers prints out the result of the first one raised by the power of the second.","The input will be two integer numbers.","The output will be the result of raising the first by the power of the second.",Playtest("2 3", "8"),Playtest("5 3","125"), false, mutableListOf()),
        Question(13,"Reverse of the integer!","Write a program that when given an integer writes it in reverse.","The input will be an integer number.","The output will be another integer that is equal to the reverse of the integer that the program received.",Playtest("356","653"),Playtest("147","741"), false, mutableListOf()),
        Question(14,"Is it prime?","Write a program that when given an integer number returns if its a prime number or not.","The input will be an integer number.","The output will be whether the integer number is a prime number or not.",Playtest("14","not prime"),Playtest("3","prime"), false, mutableListOf()),
        Question(15,"Extremes!","Write a program that when given a list of numbers returns the biggest and smallest numbers inside it.","The input will be a list of numbers that ends with 0.","The output will be the biggest number followed by the smallest (without taking in account the 0).",Playtest("15 -23 2 57 89 6 3 0","89 -23"),Playtest("2 6 -1 9 -5 15 -230 16 0","16 -230"), false, mutableListOf()),
        Question(16,"Sum of numbers!","Write a program that when given a series of numbers returns the sum of them all.","The input will be a list of integer numbers.","The output will be the sum of all integers.",Playtest("1 7 3 2 4 7 5 8 7","44"),Playtest("-3 10 1 -2 8","14"), false, mutableListOf()),
        Question(17,"Reverse the array!","Write a program that when given an array of integer numbers returns its inverted version.","The input will be an array of integer numbers.","The output will be the inverted array.",Playtest("5 4 123 345 65 324 1 2 4 66","66 4 2 1 324 65 345 123 4 5"),Playtest("0 9 8 7 6 5 4 3 2 1","1 2 3 4 5 6 7 8 9 0"), false, mutableListOf()),
        Question(18,"Which one is missing?","Write a program that when given a sequence of integer numbers returns the number that is missing.","The input will be an ordered sequence of integer numbers.","The output will be the missing number of the sequence.",Playtest("4 5 7 8 9 10 11","6"),Playtest("1 2 3 4 5 6 7 9 10 11","8"),false, mutableListOf()),
        Question(19,"Are they the same?","Write a program that when given two words returns if they are equal or not.","The input will be two words.","The output will be whether the words are equals or not.",Playtest("hey hay","not equals"),Playtest("bye bye","equals"),false, mutableListOf()),
        Question(20,"Palindrome!","Write a program that when given a word returns if its a palindrome or not.","The input will be a word.","The output will be whether the word is a palindrome or not.",Playtest("wow","yes"),Playtest("damn","no"),false, mutableListOf())
    )
    var questionsNum = 1
    jsonToList()
    try {
        val sentenciaProblem = "INSERT INTO problem VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

        val sentenciaTries = "INSERT INTO tries VALUES (?, ?, ?)"

        //Preparamos la query y ubicamos a la sentencia dentro de ella
        val consultaProblem = connect.prepareStatement(sentenciaProblem)

        val consultaTries = connect.prepareStatement(sentenciaTries)

        for (i in quest1.indices){
            consultaProblem.setInt(1, quest1[i].numberID)
            consultaProblem.setString(2, quest1[i].title)
            consultaProblem.setString(3, quest1[i].statement)
            consultaProblem.setString(4, quest1[i].inputMeasures)
            consultaProblem.setString(5, quest1[i].outputMeasures)
            consultaProblem.setString(6, quest1[i].publicPlaytest.input)
            consultaProblem.setString(7, quest1[i].publicPlaytest.output)
            consultaProblem.setString(8, quest1[i].privatePlaytest.input)
            consultaProblem.setString(9, quest1[i].privatePlaytest.output)
            consultaProblem.setBoolean(10, quest1[i].isResolved)
            consultaProblem.setInt(11, quest1[i].tries.size)
            consultaProblem.executeUpdate()

            if (quest1[i].tries.size > 0){
                for (j in quest1[i].tries.indices){
                    consultaTries.setInt(1, questionsNum++)
                    consultaTries.setString(2, quest1[i].tries[j])
                    consultaTries.setInt(3, quest1[i].numberID)
                    consultaTries.executeUpdate()
                }
            }
        }
    } catch (e: SQLException){
        println("Error:" + e.errorCode + e.message)
    } catch (e: PSQLException){
        println("Error:" + e.errorCode + e.message)
    }
}