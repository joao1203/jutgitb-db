import kotlinx.serialization.Serializable

@Serializable
data class Playtest(
    val input: String,
    val output: String
)
